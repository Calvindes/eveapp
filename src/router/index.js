import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Chars from '../views/Chars.vue';
import Corps from '../views/Corps.vue';
import Alliances from '../views/Alliances.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Chars',
    name: 'Chars',
    component: Chars
  },
  {
    path: '/Corps',
    name: 'Corps',
    component: Corps
  },
  {
    path: '/Alliances',
    name: 'Alliances',
    component: Alliances
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
